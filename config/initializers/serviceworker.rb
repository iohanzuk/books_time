Rails.application.configure do
  config.serviceworker.routes.draw do
    match "/serviceworker.js" => "path/to/precompiled/serviceworker"
  end
end