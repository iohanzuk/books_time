# Be sure to restart your server when you modify this file.

# Version of your assets, change this if you want to expire all your assets.
Rails.application.config.assets.version = '1.0'

# Add additional assets to the asset load path.
# Rails.application.config.assets.paths << Emoji.images_path
# Add Yarn node_modules folder to the asset load path.
Rails.application.config.assets.paths << Rails.root.join('node_modules')

# Precompile additional assets.
# application.js, application.css, and all non-JS/CSS in the app/assets
# folder are already added.
# Rails.application.config.assets.precompile += %w( admin.js admin.css )

Rails.application.config.assets.precompile += %w( bootstrap/bootstrap.min.css )
Rails.application.config.assets.precompile += %w( font-awesome/css/font-awesome.min )
Rails.application.config.assets.precompile += %w( Ionicons/css/ionicons.min )
Rails.application.config.assets.precompile += %w( admin/AdminLTE.min )
Rails.application.config.assets.precompile += %w( admin/skins/skin-blue )
Rails.application.config.assets.precompile += %w( datepicker/bootstrap-datepicker.min )

Rails.application.config.assets.precompile += %w( jquery.min )
Rails.application.config.assets.precompile += %w( jquery.slimscroll.min )
Rails.application.config.assets.precompile += %w( bootstrap.min )
Rails.application.config.assets.precompile += %w( fastclick )
Rails.application.config.assets.precompile += %w( pages/dashboard2 )
Rails.application.config.assets.precompile += %w( datepicker/js/bootstrap-datepicker.min )
Rails.application.config.assets.precompile += %w( adminlte.min )
Rails.application.config.assets.precompile += %w( demo )
