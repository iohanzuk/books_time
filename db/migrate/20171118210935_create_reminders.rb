class CreateReminders < ActiveRecord::Migration[5.1]
  def change
    create_table :reminders do |t|
      t.datetime :date
      t.references :book
      t.timestamps
    end
  end
end
