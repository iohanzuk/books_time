class Reminder < ApplicationRecord
  belongs_to :book
  validates_presence_of :book, :date
end
