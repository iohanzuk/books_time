json.extract! reminder, :id, :date, :created_at, :updated_at
json.url reminder_url(reminder, format: :json)
