class MetadataController < ApplicationController
  layout false

  def manifest
    manifest = {
        name: 'Books Time',
        short_name: 'BkTime',
        display: 'standalone',
        start_url:'/',
        orientation: 'portrait',
        icons:[
            {
                src: 'images/logo.png',
                sizes: '144x144',
                type: 'image/png'
            }
        ],
        gm_sender_id:  'xxxxxx'
    }

    render json: manifest.to_json
  end
end