class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  @decodedVapidPublicKey = Base64.urlsafe_decode64('BEMZ2BN-VwYbvzGIsKSeqdWjxnnZ6OFxZNBqCQtJX0gXV005SzIZoJ9EIHwBDe1x8h9LU9_HlAMKquW8EWvDycg=').bytes
end
